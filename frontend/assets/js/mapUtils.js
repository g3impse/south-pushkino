let mapboxgl;
if (process.browser) {
    mapboxgl = require('mapbox-gl');
}

export const mapToken = 'pk.eyJ1Ijoicm9zZXVyb2NpdHkiLCJhIjoiY2s2YXVjMmp4MDJ1czNqcGJvNjZ3NDU2dyJ9.MLNxmGlPSdfcn0uZ-qgovQ';

export const mapStyles = 'mapbox://styles/roseurocity/ck6av6jsl025o1jnt9r8htmgr';

export function revertCords(coords) {
    if (Array.isArray(coords) && coords.length === 2) {
        return [Number(coords[1]), Number(coords[0])];
    } else {
        console.warn('Cannot revert coords');
    }
}

export function generateMarker(className, html) {
    const el = document.createElement('DIV');
    el.classList.add(className || 'mb-marker');
    el.innerHTML = html || '<div class="mb-marker__icn"></div>';
    return el;
}

export function parseCords(coords) {
    if (typeof coords !== 'string') {
        console.warn('Coords are not a String');
        return;
    }
    let c = coords.split(',');
    if (c.length === 2) {
        return [Number(c[1]), Number(c[0])];
    } else {
        console.warn('Something wrong with coords');
    }
}

export const getRadialCoords = (center, radiusInKm, points = 64) => {
    const result = [];

    const distanceX = radiusInKm / (111.320 * Math.cos(center[1] * Math.PI / 180));
    const distanceY = radiusInKm / 110.574;

    for (let i = 0; i < points; i++) {
        const theta = (i / points) * (2 * Math.PI);
        const x = distanceX * Math.cos(theta);
        const y = distanceY * Math.sin(theta);

        result.push([center[0] + x, center[1] + y]);
    }

    return result;
};

export function generateInfraGeoJson(types) {
    if (!Array.isArray(types)) return;

    const infraIconTemplate = (coords, name, address, icon) => {
        return {
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: coords,
            },
            properties: {
                name: name,
                // address: address,
                icon: icon,
            },
        };
    };

    let arr = [];
    for (let i = 0; i < types.length; i++) {
        let type = types[i];
        let typeObj = {
            id: type.id,
            json: {
                'type': 'FeatureCollection',
                'features': [],
            },
        };

        for (let y = 0; y < type.mapplaceSet.length; y++) {
            let infra = type.mapplaceSet[y];
            let item = infraIconTemplate([infra.longitude, infra.latitude], infra.name, type.icon);

            typeObj.json.features.push(item);
        }

        arr.push(typeObj);
    }

    return arr;
}

export function addDistrict(map) {
    const POLYGON_COLOR = '#000000';

    let hoveredStateId = null;
    console.log('add');

    map.addLayer({
        'id': 'zones',
        'type': 'fill',
        'source': {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    {
                        'id': 1,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.823170, 55.988550],
                                [37.428024, 55.823716],
                                [37.424261, 55.823577],
                                [37.424245, 55.821446],
                                [37.425271, 55.820564],
                                [37.427078, 55.819856],
                                [37.430063, 55.823233],
                            ]],
                        },
                        'properties': {
                            'popup': '',
                            'title': '',
                        },
                    },
                    {
                        'id': 2,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.42688023583969, 55.81971900324993],
                                [37.42429890560663, 55.81729039323952],
                                [37.42124591965512, 55.818879353779636],
                                [37.42341719108674, 55.81972324522343],
                                [37.42572742803645, 55.82014308758795],
                                [37.42688023583969, 55.81971900324993],
                            ]],
                        },
                        'properties': {
                            'popup': '[37.42433030041752, 55.81897796106]',
                            'title': 'Проектируемая школа',
                        },
                    },
                    {
                        'id': 3,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.42402203417555, 55.8171388039645],
                                [37.42020252383577, 55.81434275119409],
                                [37.41870711588888, 55.81598228827684],
                                [37.42211388674764, 55.81811635460127],
                                [37.42402203417555, 55.8171388039645],
                            ]],
                        },
                        'properties': {
                            'popup': '[37.421326226287306, 55.8163739676331]',
                            'title': 'Проектируемый детский сад',
                        },
                    },
                    {
                        'id': 4,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.421828010133254, 55.818253141271555],
                                [37.418583695866346, 55.816208470988045],
                                [37.41765271871907, 55.8181421929269],
                                [37.42096235188137, 55.81875680133302],
                                [37.421828010133254, 55.818253141271555],
                            ]],
                        },
                        'properties': {
                            'popup': '',
                            'title': '',
                        },
                    },
                    {
                        'id': 5,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.4253697270384, 55.82032221349334],
                                [37.423231356865585, 55.8199447335173],
                                [37.421015458220495, 55.819070154865216],
                                [37.41849288939238, 55.821995249340404],
                                [37.423960934491106, 55.822241171705116],
                                [37.42393701181595, 55.82142945640584],
                                [37.425070737396254, 55.82043455351561],
                                [37.4253697270384, 55.82032221349334],
                            ]],
                        },
                        'properties': {
                            'popup': '[37.420253342712385, 55.8213889107227]',
                            'title': 'Проектируемая школа <br> и детский сад',
                        },
                    },
                    {
                        'id': 6,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.420699793592235, 55.81894465209456],
                                [37.417552981580684, 55.818398302958116],
                                [37.4174346186046, 55.820164094162976],
                                [37.419469088482856, 55.82040049057201],
                                [37.420699793592235, 55.81894465209456],
                            ]],
                        },
                        'properties': {
                            'popup': '',
                            'title': '',
                        },
                    },
                    {
                        'id': 7,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.419366845537525, 55.82051918322267],
                                [37.41743806529996, 55.82033542048691],
                                [37.417163232568896, 55.82193023081311],
                                [37.41821875309415, 55.821979008436216],
                                [37.419366845537525, 55.82051918322267],
                            ]],
                        },
                        'properties': {
                            'popup': '',
                            'title': '',
                        },
                    },
                    {
                        'id': 8,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.42397480999546, 55.82237537678432],
                                [37.42101403695813, 55.82226365834677],
                                [37.42089056661348, 55.822887210069496],
                                [37.41632008246029, 55.82281488653811],
                                [37.41666061415981, 55.824318706162444],
                                [37.423992291405625, 55.8235740918052],
                                [37.42397480999546, 55.82237537678432],
                            ]],
                        },
                        'properties': {
                            'popup': '[37.41806466015032,55.823486315403414]',
                            'title': 'Проектируемый спортивный комплекс',
                        },
                    },
                    {
                        'id': 9,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.42073351145632, 55.82224930359058],
                                [37.41991105050022, 55.82221302869232],
                                [37.419836237719664, 55.822740505470705],
                                [37.42063478911257, 55.822757467656544],
                                [37.42073351145632, 55.82224930359058],
                            ]],
                        },
                        'properties': {
                            'popup': '[37.42029625805412, 55.82254611349609]',
                            'title': 'Проектируемая поликлиника',
                        },
                    },
                    {
                        'id': 10,
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': [[
                                [37.41973692505371, 55.82220114776277],
                                [37.416333028321105, 55.82206645977638],
                                [37.41630084900362, 55.82266460418899],
                                [37.41965932092171, 55.822738865323544],
                                [37.41973692505371, 55.82220114776277],
                            ]],
                        },
                        'properties': {
                            'popup': '',
                            'title': '',
                        },
                    },
                ],
            },
        },
        'layout': {},
        'paint': {
            'fill-color': POLYGON_COLOR,
            'fill-opacity': [
                'case',
                ['boolean', ['feature-state', 'hover'], false],
                1,
                0.8,
            ],
        },
    });

    // let popup;
    // if (mapboxgl) {
    //     popup = new mapboxgl.Popup({
    //         closeButton: false,
    //         closeOnClick: false,
    //         className: 'district-popup',
    //     });
    // } else {
    //     console.warn('Mapbox in trouble');
    // }


    map.on('mouseenter', 'zones', function (e) {
        // Hover
        if (e.features.length > 0) {
            if (hoveredStateId) {
                map.setFeatureState({source: 'zones', id: hoveredStateId}, {hover: false});
            }
            hoveredStateId = e.features[0].id;
            map.setFeatureState({source: 'zones', id: hoveredStateId}, {hover: true});
        }

        // Popup
        // map.getCanvas().style.cursor = 'pointer';
        //
        // let coordinates = e.features[0].properties.popup;
        // let description = e.features[0].properties.title;
        //
        // if (coordinates && description) {
        //     popup.setLngLat(JSON.parse(coordinates))
        //         .setHTML(description)
        //         .addTo(map);
        // }
    });
    map.on('mouseleave', 'zones', function () {
        // Hover
        if (hoveredStateId) {
            map.setFeatureState({source: 'zones', id: hoveredStateId}, {hover: false});
        }
        hoveredStateId = null;
        console.log('mouseleave');

        // Popup
        // map.getCanvas().style.cursor = '';
        // if (popup) {
        //     popup.remove();
        // }
    });
}
