export function splitThousands(number) {
    return number
        .toString()
        .replace(/\D/g, '')
        .replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}

export function roundToMillions(price, accuracy = 1) {
    if (price) {
        return (Number(price) / 1000000).toFixed(accuracy);
    } else {
        return 0;
    }
}

export function plural(number, postfixes) {
    let n = Math.abs(number);
    n %= 100;
    if (n >= 5 && n <= 20) {
        return postfixes[2];
    }
    n %= 10;
    if (n === 1) {
        return postfixes[0];
    }
    if (n >= 2 && n <= 4) {
        return postfixes[1];
    }
    return postfixes[2];
}

export function getCookie(name) {
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.trim().split(';');
    let value = undefined;
    ca.forEach(item => {
        let param = item.split('=');
        if (param[0].trim() === name) {
            value = param[1];
        }
    });
    return value;
}

export function cleanPhone(value) {
    return value.replace(/ |-|\(|\)|_/g, '');
}

export function chunkArray(arr, chunkSize) {
    let copy = arr.slice(0);
    let results = [];

    while (copy.length) {
        results.push(copy.splice(0, chunkSize));
    }

    return results;
}

// export function queryToObject(qs) {
//     let obj = {};
//
//     if (qs) {
//         let params = qs.split('&');
//
//         params.forEach(param => {
//             let name = param.split('=')[0];
//             let value = param.split('=')[1];
//             if (name && value) {
//                 if (obj.hasOwnProperty(name)) {
//                     if (Array.isArray(obj[name])) {
//                         obj[name].push(value);
//                     } else {
//                         obj[name] = [obj[name], value];
//                     }
//                 } else {
//                     obj[name] = value;
//                 }
//             }
//         });
//     }
//     return obj;
// }

export function objectToQuery(obj) {
    let qs = '';
    for (let name in obj) {
        if (obj[name]) {
            if (Array.isArray(obj[name])) {
                obj[name].forEach(val => {
                    if (val) {
                        qs += `${name}=${val}&`;
                    }
                });
            } else {
                qs += `${name}=${obj[name]}&`;
            }
        }
    }
    return qs.slice(0, -1);
}

export function applyQuery(defaulValues, queryValues) {
    if (typeof defaulValues !== 'object') {
        console.warn('defaulValues must be an object');
        return;
    }
    if (typeof queryValues !== 'object') {
        console.warn('queryValues must be an object');
        return;
    }
    if (!Object.keys(queryValues).length) {
        return defaulValues;
    }

    for (let name in queryValues) {
        if (Object.prototype.hasOwnProperty.call(defaulValues, name)) {
            if (Array.isArray(defaulValues[name])) {
                if (Array.isArray(queryValues[name])) {
                    defaulValues[name] = queryValues[name];
                } else {
                    defaulValues[name] = [queryValues[name]];
                }
            } else {
                if (Array.isArray(queryValues[name])) {
                    defaulValues[name] = queryValues[name][0];
                } else {
                    defaulValues[name] = queryValues[name];
                }
            }
        }
    }

    return defaulValues;
}

export function scrollbarWidth() {
    if (typeof document === 'undefined') return 0;

    const div = document.createElement('div');

    div.style.position = 'fixed';
    div.style.left = 0;
    div.style.visibility = 'hidden';
    div.style.overflowY = 'scroll';

    document.body.appendChild(div);
    const width = div.getBoundingClientRect().right;
    document.body.removeChild(div);

    return width;
}

export function getOffset(el) {
    let rect = el.getBoundingClientRect();

    return {
        top: rect.top + window.pageYOffset,
        left: rect.left + window.pageXOffset,
    };
}

export function bytesToSize(bytes) {
    if (!bytes && bytes === 0) return '';
    const sizes = ['Байт', 'Кб', 'Мб', 'Гб', 'Тб'];
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
    if (i === 0) return `${bytes} ${sizes[i]}`;
    return `${(bytes / 1024 ** i).toFixed(1)} ${sizes[i]}`;
}

export function monthByNumber(num, options = {}) {
    if (!num) return '';

    const months = {
        0: {
            full: 'Январь',
            short: 'Янв',
            case: 'Января',
        },
        1: {
            full: 'Февраль',
            short: 'Фев',
            case: 'Февраля',
        },
        2: {
            full: 'Март',
            short: 'Мар',
            case: 'Марта',
        },
        3: {
            full: 'Апрель',
            short: 'Апр',
            case: 'Апреля',
        },
        4: {
            full: 'Май',
            short: 'Май',
            case: 'Мая',
        },
        5: {
            full: 'Июнь',
            short: 'Июн',
            case: 'Июня',
        },
        6: {
            full: 'Июль',
            short: 'Июл',
            case: 'Июля',
        },
        7: {
            full: 'Август',
            short: 'Авг',
            case: 'Августа',
        },
        8: {
            full: 'Сентябрь',
            short: 'Сен',
            case: 'Сентября',
        },
        9: {
            full: 'Октябрь',
            short: 'Окт',
            case: 'Октября',
        },
        10: {
            full: 'Ноябрь',
            short: 'Ноя',
            case: 'Ноября',
        },
        11: {
            full: 'Декабрь',
            short: 'Дек',
            case: 'Декабря',
        },
    };

    if (options.short) {
        return months[num].short;
    } else if (options.case) {
        return months[num].case;
    } else {
        return months[num].full;
    }
}

export function truncText(el, t) {
    let text = t || el.textContent;
    let wordArray = text.split(' ');

    while (el.scrollHeight > el.clientHeight) {
        wordArray.pop();
        el.innerHTML = wordArray.join(' ') + '...';
    }
}

export function scrollTo(id = false, offset = 0) {
    if (!id) {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
    } else {
        const target = document.getElementById(id);
        if (target) {
            const position = getOffset(target).top;
            window.scroll({top: position - offset, left: 0, behavior: 'smooth'});
        }
    }
}

export function addResizeListener(element, fn) {
    if (typeof document === 'undefined') return;

    if (!element.__resizeListeners__) {
        element.__resizeListeners__ = [];
        element.__ro__ = new ResizeObserver(resizeHandler);
        element.__ro__.observe(element);
    }
    element.__resizeListeners__.push(fn);
}

export function removeResizeListener(element, fn) {
    if (!element || !element.__resizeListeners__) return;
    element.__resizeListeners__.splice(
        element.__resizeListeners__.indexOf(fn),
        1,
    );
    if (!element.__resizeListeners__.length) {
        element.__ro__.disconnect();
    }
}

function resizeHandler(entries) {
    for (let entry of entries) {
        const listeners = entry.target.__resizeListeners__ || [];
        if (listeners.length) {
            listeners.forEach(fn => {
                fn();
            });
        }
    }
}

export const lockBody = () => {
    let scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;
    document.body.classList.add('_locked');
    document.body.style.paddingRight = `${scrollbarWidth}px`;
    document.body.style.height = '100%';
    document.body.style.overflow = 'hidden';
    document.documentElement.style.height = '100%';
};

export const unlockBody = () => {
    document.documentElement.style.height = '';
    document.body.style.height = '';
    document.body.style.overflow = '';
    document.body.style.paddingRight = '';
    document.body.classList.remove('_locked');
};
