let mp = null;

import {
    getArithmeticMeanCoords,
    parseCords,
    pointsCrossover
} from './helpers';

export class MapboxClusterMarker {
    constructor({mapbox, map, points, pinSize, pointRenderer, clusterRenderer}) {
        mp = mapbox;

        this.map = map;
        this.initialPoints = points.slice();

        this.pinSize = pinSize;

        this.points = [];
        this.clusters = [];
        this.clusterPointsIds = [];

        this.pointsMarkers = {};
        this.clustersMarkers = [];

        this.pointRenderer = pointRenderer;
        this.clusterRenderer = clusterRenderer;

        this.flags = {
            inProcess: false
        };

        this._init();
    }

    _init() {
        this._setPointsMarkers();
        this._addListeners();
    }

    _addListeners() {
        this.map.on('zoom', () => {
            this._calculateClusters();
        });
    }

    _calculateClusters() {
        if (this.flags.inProcess) return false;

        this.flags.inProcess = true;

        this.clusterPointsIds = [];

        // Set pin half size
        let ph = this.pinSize / 2 - 5;

        // Prepare points view coords
        for (let i = 0; i < this.initialPoints.length; i++) {
            let coords = [this.initialPoints[i].longitude, this.initialPoints[i].latitude];
            this.initialPoints[i].viewCoords = this.map.project(coords);
        }

        let preClusters = [];

        // Get crossover points for each
        for (let i = 0; i < this.initialPoints.length; i++) {
            let ic = this.initialPoints[i].viewCoords;

            let separatedPoints = this.initialPoints.filter(p => {
                let pc = p.viewCoords;

                return pointsCrossover(
                    [ic.x - ph, ic.y - ph],
                    [ic.x + ph, ic.y + ph],
                    [pc.x - ph, pc.y - ph],
                    [pc.x + ph, pc.y + ph]
                );
            });

            let ids = separatedPoints.map(p => p.id);

            if (ids.length > 1) {
                if (preClusters.length) {
                    preClusters.forEach((cluster, index) => {
                        let separated = cluster.some(c => ids.includes(c));

                        if (separated) {
                            ids = [...new Set(cluster.concat(ids))];
                            preClusters = preClusters.filter((el, y) => y !== index);
                        }
                    });
                }

                preClusters.push(ids);
            }
        }

        preClusters.forEach(c => {
            this.clusterPointsIds.push([...new Set(c)]);
        });

        this._dividePointsAndClusters();
        this.flags.inProcess = false;
    }

    _dividePointsAndClusters() {
        // Reset all points and clusters
        this.points = [];
        this.clusters = [];

        // Set points arr
        let clustersPoints = this.clusterPointsIds.reduce((acc, item) => acc.concat(item), []);
        this.points = this.initialPoints.filter(p => !clustersPoints.includes(p.id));

        // Set clusters arr
        for (let i = 0; i < this.clusterPointsIds.length; i++) {
            let newCluster = {};
            let pointsIds = this.clusterPointsIds[i];
            let points = this.initialPoints.filter(p => pointsIds.includes(p.id));

            let coords = getArithmeticMeanCoords(points);

            newCluster = {
                id: i,
                count: points.length,
                points: points,
                coords: coords
            };

            this.clusters.push(newCluster);
        }

        this._updatePointMarkers(this.points);
        // this._setClusterMarkers(this.clusters);
    }

    // Map methods
    _setPointsMarkers() {
        if (!this.pointRenderer) return false;
        this.pointsMarkers = {};

        for (let i = 0; i < this.initialPoints.length; i++) {
            let point = this.initialPoints[i];

            let marker = new mp.Marker(this.pointRenderer(point))
                .setLngLat([point.longitude, point.latitude])
                .addTo(this.map);

            this.pointsMarkers[point.id] = marker;
        }
    }

    _updatePointMarkers(points) {
        let pointsIds = points.map(p => p.id);


        Object.keys(this.pointsMarkers).forEach(id => {
            pointsIds.includes(id)
                ? this._showMarker(this.pointsMarkers[id])
                : this._hideMarker(this.pointsMarkers[id]);


        });
    }

    _hideMarker(marker) {
        marker.getElement().style.display = 'none';
    }

    _showMarker(marker) {
        marker.getElement().style.display = '';
    }

    _setClusterMarkers(clusters) {
        this.clustersMarkers.forEach(cl => cl.remove());
        this.clustersMarkers = [];

        for (let i = 0; i < clusters.length; i++) {
            let cluster = clusters[i];
            let marker = new mp.Marker(this.clusterRenderer(cluster))
                .setLngLat([cluster.longitude, cluster.latitude])
                .addTo(this.map);

            marker.getElement().addEventListener('click', () => {
                let bounds = new mp.LngLatBounds();

                cluster.points.forEach(p => {
                    bounds.extend([p.longitude, p.latitude]);
                });

                this.map.fitBounds(bounds, {
                    animate: true,
                    maxZoom: 18,
                    padding: {
                        top: 100,
                        bottom: 100,
                        left: 100,
                        right: 100
                    }
                });
            });

            this.clustersMarkers.push(marker);
        }
    }

    // Clear methods

    _removePoints() {
        Object.values(this.pointsMarkers).forEach(i => i.remove());
        this.pointsMarkers = {};
    }

    _removeClusters() {
        this.clustersMarkers.forEach(i => i.remove());
        this.clustersMarkers = [];
    }

    // Methods

    setPoints(arr) {
        this._removePoints();
        this._removeClusters();

        this.initialPoints = arr.slice();
        this._setPointsMarkers();
        this._calculateClusters();
    }

    getMarkers() {
        return this.pointsMarkers;
    }
}