const parseCords = coords => {
    if (typeof coords !== 'string') {
        console.warn('Coords are not a String');
        return;
    }

    let c = coords.split(',');
    if (c.length === 2) {
        return [Number(c[1]), Number(c[0])];
    } else {
        console.warn('Something wrong with coords');
    }
};

const pointsCrossover = ([x1, y1], [x2, y2], [x3, y3], [x4, y4]) => {
    let left = Math.max(x1, x3);
    let top = Math.min(y2, y4);
    let right = Math.min(x2, x4);
    let bottom = Math.max(y1, y3);

    let width = right - left;
    let height = top - bottom;

    return (width > 0) && (height > 0);
};

const getArithmeticMeanCoords = points => {
    let pointsCoords = [];

    for (let i = 0; i < points.length; i++) {
        let coords = [points[i].longitude, points[i].latitude];

        pointsCoords.push(coords);
    }

    let lng = pointsCoords.reduce((acc, coords) => acc + coords[0], 0) / pointsCoords.length;
    let lat = pointsCoords.reduce((acc, coords) => acc + coords[1], 0) / pointsCoords.length;

    return (`${lat},${lng}`);
};

export {
    parseCords,
    pointsCrossover,
    getArithmeticMeanCoords
};
