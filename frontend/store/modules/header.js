const DEFAULT_COLOR = '_black';
let timeOut = null;

const state = () => ({
    color: DEFAULT_COLOR,
});

const actions = {
    async CHANGE_COLOR({state, rootState, commit}, payload = {}) {
        let color = payload.color;
        commit('CHANGE_HEADER_COLOR', color);
    },
};

const mutations = {
    CHANGE_HEADER_COLOR(state, value) {
        state.color = value;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
