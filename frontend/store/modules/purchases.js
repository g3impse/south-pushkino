import allPurchaseMethods from '../../queries/purchases/allPurchaseMethods.gql';


export const state = () => ({
    purchasesList: []
});

export const getters = {
    getPurchaseDescription: state => slug => {
        let el =  state.purchasesList.find(item => item.slug === slug);
        return el?.description ? el.description : '';
    },

    getPurchaseTitle: state => slug => {
        let el = state.purchasesList.find(item => item.slug === slug);
        return el?.name ? el.name : '';
    }
};

export const actions = {
    async getPurchases({ commit }) {
        try {
            const { data } = await this.$gql.query({
                query: allPurchaseMethods
            });

            if (!data) return;

            commit('SET_PURCHASES', data.allPurchaseMethods);

        } catch (e) {
            console.log(e);
        }
    }
};
export const mutations = {
    SET_PURCHASES(state, payload) {
        state.purchasesList = [...payload];
    }
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
};
