const state = () => ({
    hidden: '',
    backgroundColor: 'white'
});

const getters = {
    hidden: state => {
        return state.hidden;
    },

    backgroundColor: state => {
        return state.backgroundColor;
    }
};

const actions = {
    async SET_HIDDEN({commit}, payload = {}) {
        commit('setHidden', payload);
    },

    async SET_COLOR({commit}, payload) {
        commit('setColor', payload);
    }
};

const mutations = {
    setHidden(state, payload) {
        state.hidden = payload;
    },

    setColor(state, payload) {
        state.backgroundColor = payload;
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};