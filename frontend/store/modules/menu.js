const DEFAULT_COLOR = '_white';
let timeOut = null;

const state = () => ({
    menuOpened: false,
    color: DEFAULT_COLOR,
    isMenuScrolled: false,
    isMenuFaded: false
});

const actions = {
    async CHANGE_COLOR_DEFAULT({state, rootState, commit}, payload = {}) {
        const {instantly} = payload;
        changeColor(commit, DEFAULT_COLOR, instantly);
    },

    CHANGE_COLOR_BLACK({state, rootState, commit}, payload = {}) {
        const {instantly} = payload;
        changeColor(commit, '_black', instantly);
    },

    CHANGE_COLOR_INDEX({state, rootState, commit}, payload = {}) {
        const {instantly} = payload;
        changeColor(commit, '_white-index', instantly);
    },

    toggleFade({state, commit}, payload) {
        commit('TOGGLE_FADE', payload);
    }
};

const mutations = {
    TOGGLE_MENU(state, newState) {
        state.menuOpened = !state.menuOpened;
    },

    TOGGLE_FADE(state, payload) {
        state.isMenuFaded = payload;
    },

    SET_MENU_STATE(state, newState) {
        state.menuOpened = newState;
    },

    SET_SCROLLED_MENU(state, newState) {
        state.isMenuScrolled = newState;
    },

    CHANGE_MENU_COLOR(state, value) {
        state.color = value;
    },
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
};

function changeColor(commitFunc, value) {

    commitFunc('CHANGE_MENU_COLOR', value);

}
