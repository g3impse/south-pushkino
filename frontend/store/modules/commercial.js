import commercialPage from '../../queries/commercial/commercialPage.gql';


export const state = () => ({
    commercial: [],
    description: ''
});


export const actions = {
    async getСommercial({ commit }) {
        try {
            const { data } = await this.$gql.query({
                query: commercialPage
            });

            if (!data) return;

            commit('SET_COMMERCIAL', data.allBuildings.edges);
            commit('SET_COMMERCIAL_DESCRIPTION', data.commercialPage);

        } catch (e) {
            console.log(e);
        }
    }
};
export const mutations = {
    SET_COMMERCIAL(state, payload) {
        let filtered = payload.map(item => item.node);
        state.commercial = [...filtered];
    },

    SET_COMMERCIAL_DESCRIPTION(state, payload) {
        state.description = payload.description;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
