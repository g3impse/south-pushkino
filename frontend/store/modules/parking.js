import parkingPage from '../../queries/parking/parkingPage.gql';


export const state = () => ({
    floors: [],
    description: ''
});


export const actions = {
    async getParkingPage({ commit }) {
        try {
            const { data } = await this.$gql.query({
                query: parkingPage
            });

            if (!data) return;

            commit('SET_PARKING_FLOORS', data.allParkingFloors);
            commit('SET_PARKING_DESCRIPTION', data.parkingPage);

        } catch (e) {
            console.log(e);
        }
    }
};
export const mutations = {
    SET_PARKING_FLOORS(state, payload) {
        state.floors = [...payload];
    },

    SET_PARKING_DESCRIPTION(state, payload) {
        state.description = payload.description;
    }

};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
