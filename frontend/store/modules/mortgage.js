const state = () => ({
    mortgageData: {},
});

const getters = {
    getMortgageData(state) {
        return state.mortgageData;
    },
};

const actions = {
    setMortgageData({commit}, payload = {}) {
        commit('SET_MORTGAGE_DATA', payload);
    },
};

const mutations = {
    SET_MORTGAGE_DATA(state, payload) {
        state.mortgageData = payload;
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};