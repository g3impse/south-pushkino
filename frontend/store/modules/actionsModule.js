import allActions from '../../queries/actions/allActions.gql';

const state = () => ({
    actionCount: 0,
});

const getters = {
    actionCount: state => {
        let actionCount;
        if (state.actionCount > 3) {
            actionCount = '+3';
        } else {
            actionCount = state.actionCount;
        }
        return actionCount;
    },
};


const actions = {
    async GET_ACTION_COUNT({commit}) {
        try {
            let actions = await this.$gql.query({
                query: allActions,
            });

            if (!actions) return;

            commit('SET_ACTION_COUNT', actions.data.allActions.edges.length);

        } catch (e) {
            console.log(e);
        }
    },
};

const mutations = {
    SET_ACTION_COUNT(state, payload) {
        state.actionCount = payload;
    },
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
};