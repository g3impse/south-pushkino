import allIndexSlides from '../../queries/home/allIndexSlides.gql';
import indexPage from '../../queries/home/indexPage.gql';
import allSections from '../../queries/home/allSections.gql';
import layoutStats from '../../queries/home/layoutStats.gql';
import allPlaceCategories from '../../queries/home/allPlaceCategories.gql';
import allMapPlaces from '../../queries/home/allMapPlaces.gql';
import Project from '../../queries/home/Project.gql';
import allFloors from '../../queries/home/allFloors.gql';

export const state = () => ({
    data: {},
    slides: [],
    floors: {},
    flats: {},
    sections: {},
    layoutStats: [],
    mapInfrastrctureCategories: [],
    mapInfrastrcturePoints: [],
    projectData: {},
});

export const getters = {
    projectCoords(state) {
        return state.projectData?.node?.latitude && state.buildings?.node?.longitude
            ? [state.projectData.node.longitude, state.buildings.node.latitude]
            : [];
    },
    projectVisual(state) {
        return {
            plan1: state.projectData?.node?.plan1 || '',
            plan1Width: state.projectData?.node?.plan1Width || '',
            plan1Height: state.projectData?.node?.plan1Height || '',
            plan2: state.projectData?.node?.plan2 || '',
            plan2Width: state.projectData?.node?.plan2Width || '',
            plan2Height: state.projectData?.node?.plan2Height || '',
            buildingSet: state.projectData?.node?.buildingSet || [],
            planplaceSet: state.projectData?.node?.planplaceSet || [],
            miniPlan: state.projectData?.node?.miniPlan || '',
            miniPlanWidth: state.projectData?.node?.miniPlanWidth || '',
            miniPlanHeight: state.projectData?.node?.miniPlanHeight || '',
        };
    },
    projectImages(state) {
        return state.projectData?.node?.projectimageSet?.length
            ? state.projectData.node.projectimageSet
            : [];
    },
};

export const actions = {
    async getSlides({commit}) {
        try {
            const {data} = await this.$gql.query({
                query: allIndexSlides,
            });

            if (!data) return;

            commit('SET_SLIDES', data.allIndexSlides);
        } catch (e) {
            console.error(e);
        }
    },

    async getLayoutStats({commit}) {
        try {
            const {data} = await this.$gql.query({
                query: layoutStats,
            });

            if (!data) return;

            commit('SET_LAYOUT_STATS', data.layoutStats);
        } catch (e) {
            console.error(e);
        }
    },

    async getMapInfrastrctureCategories({commit}) {
        try {
            const {data} = await this.$gql.query({
                query: allPlaceCategories,
            });

            const {points} = await this.$gql.query({
                query: allMapPlaces,
            });

            if (!data) return;

            commit('SET_MAP_INFRA_CATEGORIES', data.allPlaceCategories);
        } catch (e) {
            console.error(e);
        }
    },
    async getMapInfrastrcturePoints({commit}) {
        try {

            const {data} = await this.$gql.query({
                query: allMapPlaces,
            });

            if (!data) return;

            commit('SET_MAP_POINTS', data.allMapPlaces);
        } catch (e) {
            console.error(e);
        }
    },

    async getIndexPage({commit}) {
        try {
            const {data} = await this.$gql.query({
                query: indexPage,
            });

            if (!data) return;

            commit('SET_DATA', data.indexPage);
        } catch (e) {
            console.error(e);
        }
    },

    async getAllSections({commit}, buildingId) {
        try {
            const {data} = await this.$gql.query({
                query: allSections,
                variables: {
                    buildingId: buildingId,
                },
            });

            if (!data) return;
            let sections = data.allSections.edges.map(item => item.node);
            commit('SET_ALL_SECTIONS',
                {
                    sections: sections,
                    buildingId: buildingId,
                });
        } catch (e) {
            console.error(e);
        }
    },

    async getAllFloors({commit}, sectionId) {
        try {
            const {data} = await this.$gql.query({
                query: allFloors,
                variables: {
                    sectionId: sectionId,
                },
            });

            if (!data) return;
            let floors = data.allFloors.edges.map(item => item.node);
            commit('SET_ALL_FLOORS',
                {
                    floors: floors,
                    sectionId: sectionId,
                });
        } catch (e) {
            console.error(e);
        }
    },

    async getAllFloorFlats({commit}, floorId) {
        try {
            const {data} = await this.$gql.query({
                query: allFloors,
                variables: {
                    floorId: floorId,
                    withFlats: true,
                },
            });

            if (!data) return;
            let flats = data.allFloors.edges.map(item => item.node)[0].flatSet || [];
            commit('SET_ALL_FLATS',
                {
                    flats: flats,
                    floorId: floorId,
                });
        } catch (e) {
            console.error(e);
        }
    },

    async getProject({commit}) {
        try {
            const {data} = await this.$gql.query({
                query: Project,
            });

            if (!data || !data.allProjects?.edges.length) return;
            commit('SET_PROJECT', data.allProjects.edges[data.allProjects.edges.length - 1]);
        } catch (e) {
            console.error(e);
        }
    },
};

export const mutations = {
    SET_SLIDES(state, payload) {
        state.slides = [...payload];
    },

    SET_DATA(state, payload) {
        state.data = {...payload};
    },

    SET_ALL_SECTIONS(state, payload) {
        state.sections[payload.buildingId] = [...payload.sections];
    },

    SET_ALL_FLOORS(state, payload) {
        state.floors[payload.sectionId] = [...payload.floors];
    },

    SET_ALL_FLATS(state, payload) {
        state.flats[payload.floorId] = [...payload.flats];
    },

    SET_LAYOUT_STATS(state, payload) {
        state.layoutStats = [...payload];
    },

    SET_MAP_INFRA_CATEGORIES(state, payload) {
        state.mapInfrastrctureCategories = [...payload];
    },

    SET_MAP_POINTS(state, payload) {
        state.mapInfrastrcturePoints = [...payload];
    },

    SET_PROJECT(state, payload) {
        state.projectData = {...payload};
    },
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
};
