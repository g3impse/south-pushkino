import home from './modules/home';
import menu from './modules/menu';
import header from './modules/header';
import footer from './modules/footer';
import favourite from './modules/favourite';
import purchases from './modules/purchases';
import parking from './modules/parking';
import commercial from './modules/commercial';
import actionsModule from './modules/actionsModule';
import mortgage from './modules/mortgage';

export const state = () => {
    return {
        loaded: false,
        date: ''
    };
};

export const modules = {
    home,
    menu,
    header,
    footer,
    favourite,
    purchases,
    parking,
    commercial,
    actionsModule,
    mortgage
};

export const getters = {
    getDate(state) {
        return state.date;
    }
};

export const actions = {
    setDate({commit}, payload) {
        commit('SET_DATE', payload);
    },

    async nuxtServerInit({state, dispatch, commit}, {route}) {
        console.log('nuxtServerInit');
        await dispatch('favourite/getFavourites', null);
        await dispatch('actionsModule/GET_ACTION_COUNT');
        // await dispatch('project/GET_PROJECT', null);
        // await dispatch('contacts/GET_CONTACTS', null);
        // await dispatch('project/GET_PROJECT_PAGES', null);
    },
};

export const mutations = {
    PAGE_LOADED() {
        this.state.loaded = true;
    },

    PAGE_NOT_LOADED() {
        this.state.loaded = false;
    },

    SET_DATE(state, payload) {
        state.date = payload;
    }
};
