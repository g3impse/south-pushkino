import Swiper from 'swiper/dist/js/swiper.min';

export default {
    props: {
        images: {
            type: Array,
            default: () => [],
        },
    },

    data() {
        return {
            sliderApp: undefined,
            active: 0,
            autoPlayTime: 3,
            slidesPerView: 1,
            direction: 'horizontal',
            parallax: false,
            touched: false,
            swiped: false,
            play: false,
            inited: false,
            loop: false,
            loopedSlides: 0,
        };
    },

    mounted() {
        this.initApp();
        this.startObserve();
    },

    computed: {
        isShowControllers() {
            return this.images?.length && this.images.length > 1;
        },
    },

    beforeDestroy() {
        this.sliderApp.destroy();
    },

    methods: {
        startObserve() {
            if (!this.$refs?.slider) return;

            let options = {
                root: null,
                threshold: .5,
            };

            let observerCallback = (entries, observer) => {
                entries.forEach(entry => {
                    this.play = entry.isIntersecting;

                    if (this.play && !this.inited) this.active = 0;
                    if (this.play) this.inited = true;
                });
            };

            let observer = new IntersectionObserver(observerCallback, options);

            observer.observe(this.$refs.slider);
        },

        initApp() {

            const settings = {
                speed: 600,
                slidesPerView: this.slidesPerView,
                direction: this.direction,
                parallax: this.parallax,
                loop: this.loop,
                loopedSlides: this.loopedSlides,
                navigation: {
                    prevEl: this.$refs?.prev?.$el || this.$refs?.prev || false,
                    nextEl: this.$refs?.next?.$el || this.$refs?.next || false,
                    hiddenClass: '_hidden',
                },
            };

            // if (this.$refs.pagination && (this.horizontalMob && this.$mq !== 'mob')) {
            //     settings.pagination = {
            //         el: this.$refs.pagination,
            //         type: 'fraction',
            //         renderFraction: renderFraction,
            //         formatFractionCurrent: formatFraction,
            //         formatFractionTotal: formatFraction,
            //     };
            // } else if (this.$refs.pagination && this.horizontalMob && this.$mq === 'mob') {
            //     settings.pagination = {
            //         el: this.$refs.pagination,
            //         type: 'bullets',
            //         dynamicBullets: true,
            //     };
            // }

            this.sliderApp = new Swiper(this.$refs.slider, settings);

            this.sliderApp.on('progress', progress => {
                let index = Math.ceil(Math.round(progress * 10000) * this.images.length / 10000);
                this.active = index && index > 1 ? index - 1 : index;
            });

            this.sliderApp.on('touchMove', () => {
                this.swiped = true;
                this.touched = true;
            });

            this.sliderApp.on('touchEnd', (evt) => {
                if (this.swiped) return;
                this.touched = false;
            });
        },

        paginationClick(index) {
            this.goTo(index);
            this.touched = true;
        },

        goTo(index) {
            let val = index;

            if (val > this.images.length - 1) val = 0;

            this.sliderApp.slideTo(val);
        },

        prevClick() {
            this.touched = true;
            this.prev();
        },

        prev() {
            this.sliderApp.slidePrev();
        },

        nextClick() {
            this.touched = true;
            this.next();
        },

        next() {
            this.sliderApp.slideNext();
        },
    },

    watch: {
        images() {
            this.$nextTick(() => {
                this.sliderApp.update();
                this.sliderApp.slideTo(0, true);
            });
        },
    },
};
