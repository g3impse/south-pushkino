import pkg from './package';
import path from 'path';
import fs from 'fs';

const StyleLintPlugin = require('stylelint-webpack-plugin');
require('dotenv').config();

import {plugins} from './config/plugins';
import {proxy} from './config/proxy';

const env = {
    gql: process.env.GRAPHQL_URL,
    proxy: process.env.PROXY_URL,
};


module.exports = {
    mode: 'universal',

    env: {
        graphqlUrl: env.gql,
    },

    polyfill: {
        features: [
            /*
                Feature without detect:

                Note:
                  This is not recommended for most polyfills
                  because the polyfill will always be loaded, parsed and executed.
            */
            {
                require: 'url-polyfill', // NPM package or require path of file
            },

            /*
                Feature with detect:

                Detection is better because the polyfill will not be
                loaded, parsed and executed if it's not necessary.
            */
            {
                require: 'intersection-observer',
                detect: () => 'IntersectionObserver' in window,
            },

            {
                require: 'remove',
                detect: () => 'remove' in window,
            },

            /*
                Feature with detect & install:

                Some polyfills require a installation step
                Hence you could supply a install function which accepts the require result
            */
            {
                require: 'smoothscroll-polyfill',

                // Detection found in source: https://github.com/iamdustan/smoothscroll/blob/master/src/smoothscroll.js
                detect: () => 'scrollBehavior' in document.documentElement.style && window.__forceSmoothScrollPolyfill__ !== true,

                // Optional install function called client side after the package is required:
                install: (smoothscroll) => smoothscroll.polyfill(),
            },
        ],
    },

    server: process.env.HTTPS_KEY && process.env.HTTPS_CERT ? {
        https: {
            key: fs.readFileSync(path.resolve(__dirname, process.env.HTTPS_KEY)),
            cert: fs.readFileSync(path.resolve(__dirname, process.env.HTTPS_CERT)),
        },
    } : {},

    render: {
        http2: {
            push: true,
        },
    },

    /**
     * Headers of the page
     **/
    head: {
        htmlAttrs: {
            lang: 'ru',
        },
        title: 'Южное Пушкино',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: pkg.description},
            {hid: 'yandex-verification', name: 'yandex-verification', content: 'cebf9ab606ac1495'},
        ],
        link: [
            // Favicons
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicons/favicon.ico',
            },
            {
                rel: 'icon',
                type: 'image/png',
                sizes: '32x32',
                href: '/favicons/favicon-32x32.png',
            },
            {
                rel: 'icon',
                type: 'image/png',
                sizes: '16x16',
                href: '/favicons/favicon-16x16.png',
            },
            {
                rel: 'apple-touch-icon',
                sizes: '180x180',
                href: '/favicons/apple-touch-icon.png',
            },
            {rel: 'manifest', href: '/favicons/site.webmanifest'},
            {
                rel: 'mask-icon',
                href: '/favicons/safari-pinned-tab.svg',
                color: '#000',
            },
        ],

        script: [
            {
                innerHTML: `
                            var __cs = __cs || [];
                            __cs.push(["setCsAccount", "HFGOGtNGKZqp8h6hlgPzDilVx_aGSiM7"]);
                        `,
            },
            {
                src: 'https://app.comagic.ru/static/cs.min.js',
                async: true,
            },
            {
                innerHTML: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                             })(window,document,'script','dataLayer','GTM-WLBN7Z4');`,
                async: true,
            },
            {
                innerHTML: `(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                           (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
                           ym(71153968, "init", {
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                webvisor:true
                           });`,
            },
            {
                innerHTML: '!function(){function t(t,e){return function(){window.carrotquestasync.push(t,arguments)}}if("undefined"==typeof carrotquest){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="//cdn.carrotquest.app/api.min.js",document.getElementsByTagName("head")[0].appendChild(e),window.carrotquest={},window.carrotquestasync=[],carrotquest.settings={};for(var n=["connect","track","identify","auth","oth","onReady","addCallback","removeCallback","trackMessageInteraction"],a=0;a<n.length;a++)carrotquest[n[a]]=t(n[a])}}(),carrotquest.connect("41040-5e95f043b3189eee6dfaa1159f");',
            },
        ],

        __dangerouslyDisableSanitizers: ['script', 'innerHTML'],
    },

    /**
     * Customize the progress-bar color
     **/
    loading: {color: '#fe7011'},

    /**
     * Global CSS
     **/
    css: [
        '~/assets/scss/vendors.scss',
        '~/assets/scss/main.scss',
    ],

    styleResources: {
        scss: '~/assets/scss/shared/*.scss',
    },

    /**
     * Plugins to load before mounting the App
     **/
    plugins,

    /**
     * Nuxt.js modules
     **/
    modules: [
        '@nuxtjs/dotenv',
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
        '@nuxtjs/style-resources',
        '@nuxtjs/apollo',
        '@nuxtjs/device',
        'nuxt-polyfill',
    ],

    router: {
        linkActiveClass: '_active-link',
        linkExactActiveClass: '_exact-link',

    },

    apollo: {
        clientConfigs: {
            default: '~/config/apollo.js',
        },
    },

    axios: {
        // See https://github.com/nuxt-community/axios-module#options
        baseURL: env.proxy || 'http://backend:8000',
        browserBaseURL: '/',
        proxy: env.proxy || false,
        // https: true,
    },

    proxy: env.proxy ? proxy() : {},

    /**
     * Build configuration
     **/
    build: {
        publicPath: '/n/',

        babel: {
            plugins: [
                ['@babel/plugin-proposal-optional-chaining'],
            ],
        },

        //analyze: true,

        /**
         * You can extend webpack config here
         **/
        postcss: {
            // Add plugin names as key and arguments as value
            // Install them before as dependencies with npm or yarn
            preset: {
                // Change the postcss-preset-env settings
                autoprefixer: {
                    grid: true,
                },
            },
        },

        extend(config, ctx) {
            // Fixes npm packages that depend on `fs` module
            config.node = {
                fs: 'empty',
            };

            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/,
                });

                config.plugins.push(
                    new StyleLintPlugin({
                        files: ['**/*.scss', '**/*.vue'],
                        failOnError: false,
                        quiet: false,
                    }),
                );
            }
        },
    },
};
