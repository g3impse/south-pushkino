import Vue from 'vue';
import VueMq from 'vue-mq';

export default ({app}) => {
    Vue.use(VueMq, {
        breakpoints: {
            xs: 769,
            s: 1025,
            // m: 1360,
            l: 1440,
            xl: Infinity,
        },
        defaultBreakpoint: 'l',
    });
};
