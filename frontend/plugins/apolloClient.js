export default ({app}, inject) => {
    const client = app.apolloProvider.defaultClient;

    inject('gql', client);
};
