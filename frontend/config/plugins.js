export const plugins = [
    '~plugins/filters',
    '~plugins/axios',
    '~plugins/api',
    '~plugins/modal',
    '~plugins/apolloClient',
    '~plugins/lazyload',
    {
        src: '~plugins/clickoutside',
        ssr: false,
    },
    {
        src: '~plugins/smoothscroll',
        ssr: false,
    },
    {
        src: '~/plugins/vue-mq.js',
        ssr: true,
    },
    {
        src: '~/plugins/vue-slider-component.js',
        ssr: false,
    },
    // {
    //     src: '~plugins/mapboxgl',
    //     ssr: false,
    // },
];
