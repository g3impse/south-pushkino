export const proxy = () => ({
    '/api/graphql/': {
        target: process.env.PROXY_URL,
    },
    '/media': process.env.PROXY_URL,
    '/m': process.env.PROXY_URL,
});
