import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory';

const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: {
        __schema: {
            types: [],
        },
    },
});

export default function (context) {
    // Set endpoint
    let endpoint = context.env.graphqlUrl ? context.env.graphqlUrl : 'http://backend:8000/api/graphql/';

    if (process.client) {
        endpoint = `${location.origin}/api/graphql/`;
    }

    // Set headers
    let headers = {};

    if (typeof context.ssrContext != 'undefined')
        headers = context.ssrContext.req.headers;

    let ua = headers['user-agent'];

    return {
        httpEndpoint: endpoint,
        httpLinkOptions: {
            credentials: 'include', // this
            headers: {
                'user-agent': ua,
                cookie: headers.cookie
            }
        },
        cache: new InMemoryCache({fragmentMatcher}),
    };
}
